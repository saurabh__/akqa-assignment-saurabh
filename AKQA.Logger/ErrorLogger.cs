﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AKQA.Logger
{
    public static class Logger
    {
        public static void LogError(string _path, Exception ex)
        {
            string FileName = "ErrorLog.txt";
            string Location = Path.Combine(_path,FileName);
            StringBuilder ErrorInfo = new StringBuilder();
            ErrorInfo.AppendLine(DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss"));
            ErrorInfo.AppendLine("Message : "+ ex.Message.ToString());
            ErrorInfo.AppendLine("Inner Exception Message : " + (!string.IsNullOrEmpty(ex.InnerException?.Message) ? ex.InnerException.Message : ""));
            ErrorInfo.AppendLine("StackTrace : " + ex.StackTrace);
            ErrorInfo.AppendLine("Source : "+ex.Source);
            ErrorInfo.AppendLine("---------------------------------------------------------");

            if (File.Exists(Location))
            {
                File.AppendAllText(Location, ErrorInfo.ToString());
            }
            else
            {
                File.WriteAllText(Location, ErrorInfo.ToString());
            }
        }
    }
}
