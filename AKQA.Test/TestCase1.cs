using NUnit.Framework;
using AKQA.ApplicationLayer.Application;
using AKQA.DTOs;
using AKQA.Models;

namespace Tests
{
    public class TestCase1
    {
        [SetUp]
        public void Setup()
        {
        }

        IWordConverterApplication application { get; }
        public TestCase1(IWordConverterApplication _application)
        {
            application = _application;
        }

        [Test]
        public void ConvertIntoWord_PersonInfo_x()
        {
            #region Input
            PersonInfoDTO Input = new PersonInfoDTO();
            Input.Name = "John Smith";
            Input.TransactionAmount = 123.45M;
            #endregion

            #region ExpectedOutput
            PersonInfoModel ExpectedOutput = new PersonInfoModel();
            ExpectedOutput.Name = "John Smith";
            ExpectedOutput.TransactionAmount = "ONE HUNDRED AND TWENTY THREE USD AND AND FORTY FIVE CENTS";
            #endregion

            PersonInfoModel ActualOutput = (PersonInfoModel)application.ConvertIntoWord(Input).Result;
            Assert.AreEqual(ExpectedOutput, ActualOutput);
        }
    }
}