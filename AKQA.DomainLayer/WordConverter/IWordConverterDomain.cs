﻿using AKQA.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace AKQA.DomainLayer.Domain
{
    public interface IWordConverterDomain
    {
        ResultSet ConvertIntoWord(PersonInfoDTO personInfo);
    }
}
