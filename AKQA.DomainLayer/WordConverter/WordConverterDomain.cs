﻿using System;
using AKQA.DTOs;
using AKQA.Models;
using AKQA.DomainLayer.Helper;

namespace AKQA.DomainLayer.Domain
{
    public class WordConverterDomain : IWordConverterDomain
    {
        public ResultSet ConvertIntoWord(PersonInfoDTO PersonInfo)
        {
            ResultSet resultSet = new ResultSet();
            string Dollars = string.Empty;
            string Cents = string.Empty;
            string FinalText = string.Empty;
            try
            {
                var arr = PersonInfo.TransactionAmount.ToString().Split('.');
                Dollars = FormatText(WordConverterHelper.Convert(Convert.ToInt32(arr[0])));
                Cents = WordConverterHelper.Convert(Convert.ToInt32(arr[1])).Replace("and","").Trim();
                FinalText = string.Format("{0} DOLLARS AND {1} CENTS", Dollars, Cents);
                resultSet.Result = new PersonInfoModel()
                {
                    Name = PersonInfo.Name,
                    TransactionAmount = FinalText
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultSet;
        }

        public string FormatText(string Word)
        {
            if (Word.StartsWith("and", StringComparison.InvariantCultureIgnoreCase))
            {
                return Word.Remove(0, 3);
            }
            else
                return Word;
        }
    }
}
