﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using AKQA.Web.UI.Helpers;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Formatting;

namespace AKQA.Web.UI.Service
{
    public class MyService
    {
        private IConfiguration Configuration { get; }

        public MyService (IConfiguration configuration)
        {
            Configuration = configuration;
        }
      
        public async Task<HttpResponseMessage> PostRequest(object data)
        {
            var json = JsonConvert.SerializeObject(data);
            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8);
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(Configuration.GetSection("BaseUrl").Value);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var url = "api/AKQATest/Convert";
                return await client.PostAsync(url, data, new JsonMediaTypeFormatter());
            }
        }

    }
}
