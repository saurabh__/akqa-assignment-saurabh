﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AKQA.Web.UI.Models;
using AKQA.Web.UI.Service;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace AKQA.Web.UI.Controllers
{
    public class HomeController : Controller
    {
        private IConfiguration Configuration { get; }

        public HomeController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(new PersonViewModel());
        }

        [HttpPost]
        public IActionResult Index(PersonViewModel ViewModel)
        {
            try
            {
                ViewModel.InfoDTO.TransactionAmount = Math.Round(ViewModel.InfoDTO.TransactionAmount, 2);
                MyService service = new MyService(Configuration);
                var result = service.PostRequest(ViewModel.InfoDTO).GetAwaiter().GetResult();
                if (result.IsSuccessStatusCode)
                {
                    var data = JsonConvert.DeserializeObject<ResultSet>(result.Content.ReadAsStringAsync().Result);
                    ViewModel.InfoModel = JsonConvert.DeserializeObject<PersonInfoModel>(data.Result.ToString());
                    if (data.StatusCode != 200 && data.StatusCode != 0)
                    {
                        ViewModel.StatusCode = data.StatusCode;
                        ViewModel.Message = data.Message;
                    }

                }
                else
                {
                    ViewModel.Message = result.ReasonPhrase;
                }
                if (ViewModel.StatusCode == 0)
                    ViewModel.StatusCode = Convert.ToInt16(result.StatusCode);
            }
            catch (Exception ex)
            {
                ViewModel.StatusCode = 415;
                ViewModel.Message = ex.Message;
            }

            return View(ViewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
