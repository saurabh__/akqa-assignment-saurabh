﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AKQA.Web.UI.Models
{
    public class PersonViewModel
    {
        public PersonInfoDTO InfoDTO { get; set; }

        public PersonInfoModel InfoModel { get; set; }

        public string Message { get; set; }

        public int StatusCode { get; set; }
    }
}
