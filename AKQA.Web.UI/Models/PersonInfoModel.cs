﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AKQA.Web.UI.Models
{
    public class PersonInfoModel
    {
        public string Name { get; set; }

        public string TransactionAmount { get; set; }

    }
}
