﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AKQA.Web.UI.Models
{
    public class ResultSet
    {
        public int StatusCode { get; set; }

        public object Result { get; set; }

        public string Message { get; set; }
    }
}
