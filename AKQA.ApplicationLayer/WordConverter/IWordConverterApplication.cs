﻿using System;
using System.Collections.Generic;
using System.Text;
using AKQA.DTOs;

namespace AKQA.ApplicationLayer.Application
{
    public interface IWordConverterApplication
    {
        ResultSet ConvertIntoWord(PersonInfoDTO personInfo);
    }
}
