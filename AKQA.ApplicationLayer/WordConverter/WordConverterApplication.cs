﻿using System;
using System.Collections.Generic;
using System.Text;
using AKQA.DTOs;
using AKQA.DomainLayer.Domain;
using System.Net;

namespace AKQA.ApplicationLayer.Application
{
    public class WordConverterApplication : IWordConverterApplication
    {
        private IWordConverterDomain WordConverterDomain { get; }

        public WordConverterApplication(IWordConverterDomain wordConverterDomain)
        {
            WordConverterDomain = wordConverterDomain;
        }

        public ResultSet ConvertIntoWord(PersonInfoDTO PersonInfo)
        {
            ResultSet resultSet = new ResultSet();
            try
            {
                //Negation Check , Although will work with -ve value but assuming that it will get supplied as +ve
                if(PersonInfo.TransactionAmount<0)
                {
                    return new ResultSet()
                    {
                        StatusCode =(int)HttpStatusCode.NotAcceptable,
                        Message = "Amount Should be Gretaer then Zero"
                    };
                }
                resultSet = WordConverterDomain.ConvertIntoWord(PersonInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultSet;
        }
    }
}
