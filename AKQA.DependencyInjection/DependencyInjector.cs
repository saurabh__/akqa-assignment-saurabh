﻿using Microsoft.Extensions.DependencyInjection;
using AKQA.ApplicationLayer.Application;
using AKQA.DomainLayer.Domain;
using System;

namespace AKQA.DependencyInjection
{
    public static class DependencyInjector
    {
        static IServiceProvider ServiceProvider { get; set; }

        static IServiceCollection Services { get; set; }

        public static IServiceCollection RegisterServices(IServiceCollection services)
        {
            #region "Domains"
            services.AddScoped<IWordConverterDomain, WordConverterDomain>();
            #endregion

            #region "Applications"
            services.AddScoped<IWordConverterApplication, WordConverterApplication>();
            #endregion
          
            Services = services;

            return Services;
        }
    }
}
