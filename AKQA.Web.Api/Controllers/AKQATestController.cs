﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AKQA.ApplicationLayer.Application;
using AKQA.DTOs;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Net;
using AKQA.Logger;

namespace AKQA.Web.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AKQATestController : Controller
    {
        private IWordConverterApplication WordConverterApplication { get; }

        public AKQATestController(IWordConverterApplication wordConverterApplication)
        {
            WordConverterApplication = wordConverterApplication;
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Convert([FromBody] PersonInfoDTO PersonInfo) //[FromBody] PersonInfoDTO PersonInfo
        {
            try
            {
                var result = WordConverterApplication.ConvertIntoWord(PersonInfo);
                //If there is no error than finally set StatusCode = 200
                if (result.StatusCode <= 0)
                {
                    result.StatusCode = (int)HttpStatusCode.OK;
                }
                return Json(result, new JsonSerializerSettings() { Formatting = Formatting.Indented, ReferenceLoopHandling = ReferenceLoopHandling.Ignore, ContractResolver = new CamelCasePropertyNamesContractResolver() });
            }
            catch (Exception ex) 
            {
                Logger.Logger.LogError("Errors", ex);
                var errorResult = new ResultSet()
                {
                    StatusCode = BadRequest().StatusCode,
                    Result = ex,
                    Message =ex.Message,
                };
                return Json(errorResult, new JsonSerializerSettings() { Formatting = Formatting.Indented, ReferenceLoopHandling = ReferenceLoopHandling.Ignore, ContractResolver = new CamelCasePropertyNamesContractResolver() });
            }
        }

    }
}