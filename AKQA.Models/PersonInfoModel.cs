﻿using System;

namespace AKQA.Models
{
    public class PersonInfoModel
    {
        public string Name { get; set; }

        public string TransactionAmount { get; set; }

    }
}
