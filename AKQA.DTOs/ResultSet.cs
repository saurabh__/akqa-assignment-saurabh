﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AKQA.DTOs
{
   public class ResultSet
    {
        public int StatusCode { get; set; }

        public object Result { get; set; }

        public string Message { get; set; }
    }
}
