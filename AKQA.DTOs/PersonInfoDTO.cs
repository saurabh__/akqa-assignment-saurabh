﻿using System;

namespace AKQA.DTOs
{
    public class PersonInfoDTO
    {
        public string Name { get; set; }
        public Decimal TransactionAmount { get; set; }
    }
}
